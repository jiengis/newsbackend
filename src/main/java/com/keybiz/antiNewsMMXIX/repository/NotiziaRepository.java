package com.keybiz.antiNewsMMXIX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.keybiz.antiNewsMMXIX.model.Notizia;

public interface NotiziaRepository extends JpaRepository<Notizia, Long> {

}
