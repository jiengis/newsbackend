package com.keybiz.antiNewsMMXIX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.keybiz.antiNewsMMXIX.model.Utente;

public interface UtenteRepository extends JpaRepository<Utente, Long> {

}
