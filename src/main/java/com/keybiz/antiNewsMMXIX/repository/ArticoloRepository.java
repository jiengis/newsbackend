package com.keybiz.antiNewsMMXIX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.keybiz.antiNewsMMXIX.model.Articolo;

public interface ArticoloRepository extends JpaRepository<Articolo, Long> {

}
