package com.keybiz.antiNewsMMXIX.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.keybiz.antiNewsMMXIX.model.Commento;

public interface CommentoRepository extends JpaRepository<Commento, Long> {

}
