package com.keybiz.antiNewsMMXIX.service;

import java.util.Collection;
import java.util.Optional;

import com.keybiz.antiNewsMMXIX.dto.NotiziaDto;
import com.keybiz.antiNewsMMXIX.model.Notizia;


public interface NotiziaService {

	public Notizia saveNotizia(NotiziaDto notiziaDto);
	
	public Optional<Notizia> searchById(Long id);
	
	public void deleteById(Long id);
	
	public Collection<Notizia> findAll();

	public Notizia updateNotizia(NotiziaDto notiziaUpdated);
}
