package com.keybiz.antiNewsMMXIX.service.impl;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import com.keybiz.antiNewsMMXIX.helper.CommentoHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keybiz.antiNewsMMXIX.dto.CommentoDto;
import com.keybiz.antiNewsMMXIX.model.Commento;
import com.keybiz.antiNewsMMXIX.repository.CommentoRepository;
import com.keybiz.antiNewsMMXIX.repository.NotiziaRepository;
import com.keybiz.antiNewsMMXIX.repository.UtenteRepository;
import com.keybiz.antiNewsMMXIX.service.CommentoService;

import lombok.RequiredArgsConstructor;


@Service
@RequiredArgsConstructor
public class CommentoServiceImpl implements CommentoService {

	private final CommentoRepository commentoRepository;
	private final UtenteRepository utenteRepository;
	private final NotiziaRepository notiziaRepository;

	
	@Override
	public Commento saveCommento(CommentoDto commentoDto) {
		Commento newComm = new Commento();
		newComm.setCommentatore(utenteRepository.findById(commentoDto.getCommentatore()).get());
		newComm.setNotiziaCommento(notiziaRepository.findById(commentoDto.getNotiziaCommento()).get());
		newComm.setTesto(commentoDto.getTesto());
		return commentoRepository.save(newComm);
	}

	public Commento updateCommento(CommentoDto commentoUpdated){
		Commento commentoToUpdate = commentoRepository.getOne(commentoUpdated.getId());
		commentoToUpdate.setTesto(commentoUpdated.getTesto());
		commentoToUpdate.setCommentatore(utenteRepository.findById(commentoUpdated.getCommentatore()).get());
		commentoToUpdate.setNotiziaCommento(notiziaRepository.findById(commentoUpdated.getNotiziaCommento()).get());
		return commentoRepository.save(commentoToUpdate);
	}

	@Override
	public Optional<Commento> searchById(Long id) {
		return commentoRepository.findById(id);
	}

	@Override
	public void deleteById(Long id) {
		commentoRepository.deleteById(id);
	}

	@Override
	public Collection<Commento> findAll() {
		return commentoRepository.findAll();
	}

}
