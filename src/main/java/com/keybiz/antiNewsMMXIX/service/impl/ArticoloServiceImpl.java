package com.keybiz.antiNewsMMXIX.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.keybiz.antiNewsMMXIX.dto.ArticoloDto;
import com.keybiz.antiNewsMMXIX.model.Articolo;
import com.keybiz.antiNewsMMXIX.repository.ArticoloRepository;
import com.keybiz.antiNewsMMXIX.repository.UtenteRepository;
import com.keybiz.antiNewsMMXIX.service.ArticoloService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ArticoloServiceImpl implements ArticoloService {

	
	private final ArticoloRepository articoloRepository;
	private final UtenteRepository utenteRepository;
	
	@Override
	public Articolo saveArticolo(ArticoloDto articoloIn) {
		Articolo newArt = new Articolo();
		newArt.setAutoreArticolo(utenteRepository.findById(articoloIn.getAutoreArticolo()).get());
		//newArt.setAutoreArticolo(utenteRepository.getOne(articoloIn.getAutoreArticolo()));
		newArt.setDataCreazione(new Date());
		newArt.setDataPubblicazione(null);
		newArt.setImmagine(articoloIn.getImmagine());
		newArt.setTitolo(articoloIn.getTitolo());
		newArt.setTesto(articoloIn.getTesto());
		newArt.setSezione(articoloIn.getSezione());
		newArt.setPrezzo(articoloIn.getPrezzo());
		newArt.setVendibile(articoloIn.isVendibile());
		return articoloRepository.save(newArt);
	}

	@Override
	public Articolo updateArticolo(ArticoloDto articoloUpdated){
		Articolo articoloToUpdate = articoloRepository.getOne(articoloUpdated.getId());
		articoloToUpdate.setDataCreazione(articoloUpdated.getDataCreazione());
		articoloToUpdate.setDataPubblicazione(articoloUpdated.getDataPubblicazione());
		articoloToUpdate.setImmagine(articoloUpdated.getImmagine());
		articoloToUpdate.setTitolo(articoloUpdated.getTitolo());
		articoloToUpdate.setTesto(articoloUpdated.getTesto());
		articoloToUpdate.setSezione(articoloUpdated.getSezione());
		articoloToUpdate.setPrezzo(articoloUpdated.getPrezzo());
		articoloToUpdate.setVendibile(articoloUpdated.isVendibile());
		return articoloRepository.save(articoloToUpdate);
	}

	@Override
	public Optional<Articolo> searchById(Long id) {
		return articoloRepository.findById(id);
	}

	@Override
	public void deleteById(Long id) {
		articoloRepository.deleteById(id);
	}

	@Override
	public Collection<Articolo> findAll() {
		return articoloRepository.findAll();
	}

}
