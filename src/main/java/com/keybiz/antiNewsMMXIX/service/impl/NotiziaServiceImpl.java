package com.keybiz.antiNewsMMXIX.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

import com.keybiz.antiNewsMMXIX.helper.NotiziaHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keybiz.antiNewsMMXIX.dto.NotiziaDto;
import com.keybiz.antiNewsMMXIX.model.Notizia;
import com.keybiz.antiNewsMMXIX.repository.NotiziaRepository;
import com.keybiz.antiNewsMMXIX.repository.UtenteRepository;
import com.keybiz.antiNewsMMXIX.service.NotiziaService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor=@__({@Autowired}))
public class NotiziaServiceImpl implements NotiziaService {

	private final NotiziaRepository notiziaRepository;
	private final UtenteRepository utenteRepository;

	@Override
	public Notizia saveNotizia(NotiziaDto notiziaDto) {
		Notizia newNoti = new Notizia();
		newNoti.setAutoreNotizia(utenteRepository.findById(notiziaDto.getAutoreNotizia()).get());
		newNoti.setCommentiNotizia(null);
		newNoti.setDataCreazione(new Date());
		newNoti.setDataPubblicazione(notiziaDto.getDataPubblicazione());
		newNoti.setDataFineVisual(notiziaDto.getDataFineVisual());
		newNoti.setImmagine(notiziaDto.getImmagine());
		newNoti.setImportanza(notiziaDto.getImportanza());
		newNoti.setRiassunto(notiziaDto.getRiassunto());
		newNoti.setSottotitolo(notiziaDto.getSottotitolo());
		newNoti.setTesto(notiziaDto.getTesto());
		newNoti.setTitolo(notiziaDto.getTitolo());
		return notiziaRepository.save(newNoti);
	}

	@Override
	public Notizia updateNotizia(NotiziaDto notiziaUpdated){
		Notizia notiziaToUpdate = notiziaRepository.getOne(notiziaUpdated.getId());
		notiziaToUpdate.setDataCreazione(notiziaUpdated.getDataCreazione());
		notiziaToUpdate.setDataPubblicazione(notiziaUpdated.getDataPubblicazione());
		notiziaToUpdate.setDataFineVisual(notiziaUpdated.getDataFineVisual());
		notiziaToUpdate.setImmagine(notiziaUpdated.getImmagine());
		notiziaToUpdate.setImportanza(notiziaUpdated.getImportanza());
		notiziaToUpdate.setRiassunto(notiziaUpdated.getRiassunto());
		notiziaToUpdate.setSottotitolo(notiziaUpdated.getSottotitolo());
		notiziaToUpdate.setTesto(notiziaUpdated.getTesto());
		notiziaToUpdate.setTitolo(notiziaUpdated.getTitolo());
		notiziaToUpdate.setAutoreNotizia(utenteRepository.findById(notiziaUpdated.getAutoreNotizia()).get());
		notiziaToUpdate.setCommentiNotizia(null);
		return notiziaRepository.save(notiziaToUpdate);
	}

	@Override
	public Optional<Notizia> searchById(Long id) {
		return notiziaRepository.findById(id);
		
	}

	@Override
	public void deleteById(Long id) {
		notiziaRepository.deleteById(id);
	}

	@Override
	public Collection<Notizia> findAll() {



		return notiziaRepository.findAll();
	}
	
}
