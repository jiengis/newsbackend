package com.keybiz.antiNewsMMXIX.service.impl;

import java.util.Collection;
import java.util.Optional;

import com.keybiz.antiNewsMMXIX.model.Commento;
import com.keybiz.antiNewsMMXIX.repository.CommentoRepository;
import org.springframework.stereotype.Service;

import com.keybiz.antiNewsMMXIX.dto.UtenteDto;
import com.keybiz.antiNewsMMXIX.model.Utente;
import com.keybiz.antiNewsMMXIX.repository.UtenteRepository;
import com.keybiz.antiNewsMMXIX.service.UtenteService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UtenteServiceImpl implements UtenteService {

	private final UtenteRepository utenteRepository;
	private final CommentoRepository commentoRepository;
	
	@Override
	public Utente saveUtente(UtenteDto utenteDto) {
		Utente newUtente = new Utente();
		newUtente.setFirstname(utenteDto.getFirstname());
		newUtente.setLastname(utenteDto.getLastname());
		newUtente.setUsername(utenteDto.getUsername());
		newUtente.setPassword(utenteDto.getPassword());
		newUtente.setDob(utenteDto.getDob());
		newUtente.setPrivileges(utenteDto.getPrivileges());
		newUtente.setContoVirtuale(utenteDto.getContoVirtuale());
		newUtente.setCommentiScritti(null);
		newUtente.setArticoliScritti(null);
		newUtente.setNotizieScritte(null);
		return utenteRepository.save(newUtente);
	}


	@Override
	public Utente updateUtente(UtenteDto utenteUpdated) {
		Utente utenteToUpdate = utenteRepository.getOne(utenteUpdated.getId());
		utenteToUpdate.setId(utenteUpdated.getId());
		utenteToUpdate.setFirstname(utenteUpdated.getFirstname());
		utenteToUpdate.setLastname(utenteUpdated.getLastname());
		utenteToUpdate.setUsername(utenteUpdated.getUsername());
		utenteToUpdate.setPassword(utenteUpdated.getPassword());
		utenteToUpdate.setDob(utenteUpdated.getDob());
		utenteToUpdate.setPrivileges(utenteUpdated.getPrivileges());
		utenteToUpdate.setContoVirtuale(utenteUpdated.getContoVirtuale());
		utenteToUpdate.setCommentiScritti(null);
		utenteToUpdate.setArticoliScritti(null);
		utenteToUpdate.setNotizieScritte(null);
		return utenteRepository.save(utenteToUpdate);

	}

	@Override
	public Optional<Utente> searchById(Long id) {
		return utenteRepository.findById(id);
	}


	@Override
	public void deleteById(Long id) {
		utenteRepository.deleteById(id);
	}

	@Override
	public Collection<Utente> findAll() {
		return utenteRepository.findAll();
	}

}
