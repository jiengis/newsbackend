package com.keybiz.antiNewsMMXIX.service;

import java.util.Collection;
import java.util.Optional;

import com.keybiz.antiNewsMMXIX.dto.CommentoDto;
import com.keybiz.antiNewsMMXIX.model.Commento;

public interface CommentoService {

	public Commento saveCommento(CommentoDto commentoDto);
	
	public Optional<Commento> searchById(Long id);
	
	public void deleteById(Long id);
	
	public Collection<Commento> findAll();

	public Commento updateCommento(CommentoDto commentoToUpdate);
}
