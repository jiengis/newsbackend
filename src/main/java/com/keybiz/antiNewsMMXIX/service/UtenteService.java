package com.keybiz.antiNewsMMXIX.service;

import java.util.Collection;
import java.util.Optional;

import com.keybiz.antiNewsMMXIX.dto.UtenteDto;
import com.keybiz.antiNewsMMXIX.model.Utente;

public interface UtenteService {

	public Utente saveUtente(UtenteDto utenteDto);
	
	public Optional<Utente> searchById(Long id);

	public void deleteById(Long id);
	
	public Collection<Utente> findAll();

	public Utente updateUtente(UtenteDto utenteUpdated);
}
