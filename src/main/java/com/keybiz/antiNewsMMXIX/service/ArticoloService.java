package com.keybiz.antiNewsMMXIX.service;

import java.util.Collection;
import java.util.Optional;

import com.keybiz.antiNewsMMXIX.dto.ArticoloDto;
import com.keybiz.antiNewsMMXIX.model.Articolo;

public interface ArticoloService {

	public Articolo saveArticolo(ArticoloDto articoloDto);
	
	public Optional<Articolo> searchById(Long id);
	
	public void deleteById(Long id);
	
	public Collection<Articolo> findAll();

	public Articolo updateArticolo(ArticoloDto articoloUpdated);

}
