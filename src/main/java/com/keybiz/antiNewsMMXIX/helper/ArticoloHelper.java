package com.keybiz.antiNewsMMXIX.helper;

import com.keybiz.antiNewsMMXIX.dto.ArticoloDto;
import com.keybiz.antiNewsMMXIX.model.Articolo;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class ArticoloHelper {

    public static ArticoloDto converti(Articolo articolo) {
        ArticoloDto articoloDto = new ArticoloDto();
        articoloDto.setId(articolo.getId());
        articoloDto.setTitolo(articolo.getTitolo());
        articoloDto.setTesto(articolo.getTesto());
        articoloDto.setImmagine(articolo.getImmagine());
        articoloDto.setDataCreazione(articolo.getDataCreazione());
        articoloDto.setDataPubblicazione(articolo.getDataPubblicazione());
        articoloDto.setSezione(articolo.getSezione());
        articoloDto.setPrezzo(articolo.getPrezzo());
        articoloDto.setVendibile(articolo.isVendibile());
        articoloDto.setAutoreArticolo(articolo.getAutoreArticolo().getId());
        return articoloDto;
    }

    public static Collection<ArticoloDto> converti(Collection<Articolo> all){
        return all.stream().map(ArticoloHelper::converti).collect(Collectors.toList());
    }
}
