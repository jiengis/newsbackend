package com.keybiz.antiNewsMMXIX.helper;

import com.keybiz.antiNewsMMXIX.dto.NotiziaDto;
import com.keybiz.antiNewsMMXIX.model.Commento;
import com.keybiz.antiNewsMMXIX.model.Notizia;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class NotiziaHelper {

    public static NotiziaDto converti(Notizia notizia) {
        NotiziaDto notiziaDto = new NotiziaDto();
        notiziaDto.setId(notizia.getId());
        notiziaDto.setTitolo(notizia.getTitolo());
        notiziaDto.setTesto(notizia.getTesto());
        notiziaDto.setImmagine(notizia.getImmagine());
        notiziaDto.setDataCreazione(notizia.getDataCreazione());
        notiziaDto.setDataPubblicazione(notizia.getDataPubblicazione());
        notiziaDto.setDataFineVisual(notizia.getDataFineVisual());
        notiziaDto.setSottotitolo(notizia.getSottotitolo());
        notiziaDto.setRiassunto(notizia.getRiassunto());
        notiziaDto.setImportanza(notizia.getImportanza());
        notiziaDto.setAutoreNotizia(notizia.getAutoreNotizia().getId());
        if(notizia.getCommentiNotizia() != null){
            notiziaDto.setCommentiNotizia(notizia.getCommentiNotizia().stream().map(Commento::getId).collect(Collectors.toList()));
        }
        return notiziaDto;
    }

    public static Collection<NotiziaDto> converti(Collection<Notizia> all){
        return all
                .stream()
                .map(NotiziaHelper::converti)
                .collect(Collectors.toList());
    }

}
