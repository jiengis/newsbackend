package com.keybiz.antiNewsMMXIX.helper;

import com.keybiz.antiNewsMMXIX.dto.CommentoDto;
import com.keybiz.antiNewsMMXIX.model.Commento;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class CommentoHelper {

    public static CommentoDto converti(Commento commento) {
        CommentoDto commentoDto = new CommentoDto();
        commentoDto.setId(commento.getId());
        commentoDto.setTesto(commento.getTesto());
        commentoDto.setCommentatore(commento.getCommentatore().getId());
        commentoDto.setNotiziaCommento(commento.getNotiziaCommento().getId());
        return commentoDto;
    }

    public static Collection<CommentoDto> converti(Collection<Commento> all){
        return all.stream().map(CommentoHelper::converti).collect(Collectors.toList());
    }
}
