package com.keybiz.antiNewsMMXIX.helper;


import com.keybiz.antiNewsMMXIX.dto.UtenteDto;
import com.keybiz.antiNewsMMXIX.model.Articolo;
import com.keybiz.antiNewsMMXIX.model.Commento;
import com.keybiz.antiNewsMMXIX.model.Notizia;
import com.keybiz.antiNewsMMXIX.model.Utente;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class UtenteHelper {


    public static UtenteDto converti(Utente utente) {
        UtenteDto utenteDto = new UtenteDto();
        utenteDto.setId(utente.getId());
        utenteDto.setFirstname(utente.getFirstname());
        utenteDto.setLastname(utente.getLastname());
        utenteDto.setUsername(utente.getUsername());
        utenteDto.setPassword(utente.getPassword());
        utenteDto.setDob(utente.getDob());
        utenteDto.setPrivileges(utente.getPrivileges());
        utenteDto.setContoVirtuale(utente.getContoVirtuale());
        if(utente.getArticoliScritti() != null){
            utenteDto.setArticoliScritti(utente.getArticoliScritti().stream().map(Articolo::getId).collect(Collectors.toList()));
        }
        if(utente.getNotizieScritte() != null){
            utenteDto.setNotizieScritte(utente.getNotizieScritte().stream().map(Notizia::getId).collect(Collectors.toList()));
        }
        if(utente.getCommentiScritti() != null){
            utenteDto.setCommentiScritti(utente.getCommentiScritti().stream().map(Commento::getId).collect(Collectors.toList()));
        }
        return utenteDto;
    }

    public static Collection<UtenteDto> converti(Collection<Utente> all) {
        return all
                .stream()
                .map(UtenteHelper::converti)
                .collect(Collectors.toList());
    }

}
