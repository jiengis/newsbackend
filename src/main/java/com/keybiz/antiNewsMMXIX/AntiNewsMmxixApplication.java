package com.keybiz.antiNewsMMXIX;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AntiNewsMmxixApplication {

	public static void main(String[] args) {
		SpringApplication.run(AntiNewsMmxixApplication.class, args);
	}

}

