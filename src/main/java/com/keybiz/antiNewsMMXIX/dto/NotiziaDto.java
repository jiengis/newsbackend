package com.keybiz.antiNewsMMXIX.dto;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import com.keybiz.antiNewsMMXIX.model.Notizia;

import lombok.Data;

@Data
public class NotiziaDto {

	private Long id;
	private String titolo;
	private String testo;
	private String immagine;
	private Date dataCreazione;
	private Date dataPubblicazione;
	private Date dataFineVisual;
	private String sottotitolo;
	private String riassunto;
	private String importanza;

	private Long autoreNotizia;
	private Collection<Long> commentiNotizia;

}
