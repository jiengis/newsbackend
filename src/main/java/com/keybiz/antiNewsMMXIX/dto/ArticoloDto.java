package com.keybiz.antiNewsMMXIX.dto;

import java.util.Date;

import lombok.Data;

@Data
public class ArticoloDto {

	private Long id;
	private String titolo;
	private String testo;
	private String immagine;
	private Date dataCreazione;
	private Date dataPubblicazione;
	private String sezione;
	private Double prezzo;
	private boolean vendibile;
	private Long autoreArticolo;
	

}
