package com.keybiz.antiNewsMMXIX.dto;

import com.keybiz.antiNewsMMXIX.model.Commento;

import lombok.Data;

@Data
public class CommentoDto {

	private Long id;
	private String testo;
	private Long commentatore;
	private Long notiziaCommento;

}
