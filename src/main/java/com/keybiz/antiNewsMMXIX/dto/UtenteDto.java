package com.keybiz.antiNewsMMXIX.dto;

import java.util.Collection;
import java.util.Date;

import lombok.Data;

@Data
public class UtenteDto {

	private Long id;
	private String firstname;
	private String lastname;
	private String username;
	private String password;
	private Date dob;
	private String privileges;
	private Double contoVirtuale;

	private Collection<Long> articoliScritti;

	private Collection<Long> notizieScritte;

	private Collection<Long> commentiScritti;

}
