package com.keybiz.antiNewsMMXIX.controller;

import java.util.Collection;
import java.util.Optional;

import com.keybiz.antiNewsMMXIX.helper.NotiziaHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.keybiz.antiNewsMMXIX.dto.NotiziaDto;
import com.keybiz.antiNewsMMXIX.model.Notizia;
import com.keybiz.antiNewsMMXIX.service.NotiziaService;

@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping("notizie")
@RestController
@RequiredArgsConstructor
public class NotiziaController {

	private final NotiziaService notiziaService;

	@PostMapping
	public ResponseEntity<?> saveNotizia(@RequestBody NotiziaDto notiziaDto){
		Notizia notizia = notiziaService.saveNotizia(notiziaDto);
		NotiziaDto notiziaResult = NotiziaHelper.converti(notizia);
		return ResponseEntity.created(null).body(notiziaResult);
	}

	@PutMapping("{id}")
	public ResponseEntity<?> updateNotizia(@RequestBody NotiziaDto notiziaUpdated, @PathVariable Long id){
		Optional<Notizia> optNotizia = notiziaService.searchById(id);
		if(!optNotizia.isPresent()){
			return ResponseEntity.notFound().build();
		}
		notiziaUpdated.setId(id);
		notiziaService.updateNotizia(notiziaUpdated);
		return ResponseEntity.ok().body(NotiziaHelper.converti(optNotizia.get()));
	}

	@GetMapping("{id}")
	public ResponseEntity<?> searchById(@PathVariable Long id){
		Optional<Notizia> notizia = notiziaService.searchById(id);
		return ResponseEntity.of(notizia.map(NotiziaHelper::converti));

	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void deleteById(@PathVariable Long id){
		notiziaService.deleteById(id);
	}


	@GetMapping
	public Collection<NotiziaDto> findAll(){
		return NotiziaHelper.converti(notiziaService.findAll());
	}
	
}
