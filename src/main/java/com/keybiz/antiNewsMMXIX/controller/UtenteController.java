package com.keybiz.antiNewsMMXIX.controller;

import com.keybiz.antiNewsMMXIX.dto.UtenteDto;
import com.keybiz.antiNewsMMXIX.helper.UtenteHelper;
import com.keybiz.antiNewsMMXIX.model.Utente;
import com.keybiz.antiNewsMMXIX.service.UtenteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping("utenti")
@RestController
@RequiredArgsConstructor
public class UtenteController {

    private final UtenteService utenteService;

    @PostMapping
    public ResponseEntity<?> saveUtente(@RequestBody UtenteDto utenteDto) {
        Utente utente = utenteService.saveUtente(utenteDto);
        UtenteDto utenteResult = UtenteHelper.converti(utente);
        return ResponseEntity.created(null).body(utenteResult);
    }


    @PutMapping("{id}")
    public ResponseEntity<?> updateUtente(@RequestBody UtenteDto utenteUpdated, @PathVariable Long id) {
        Optional<Utente> optUtente = utenteService.searchById(id);
        if (!optUtente.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        utenteUpdated.setId(id);
        utenteService.updateUtente(utenteUpdated);
        return ResponseEntity.ok().body(UtenteHelper.converti(optUtente.get()));
    }


    @GetMapping("{id}")
    public ResponseEntity<?> searchById(@PathVariable Long id) {
        Optional<Utente> utente = utenteService.searchById(id);
        return ResponseEntity.of(utente.map(UtenteHelper::converti));
    }


    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteById(@PathVariable Long id) {
        utenteService.deleteById(id);
    }

    @GetMapping()
    public Collection<UtenteDto> findAll() {
        return UtenteHelper.converti(utenteService.findAll());
    }

}
