package com.keybiz.antiNewsMMXIX.controller;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import com.keybiz.antiNewsMMXIX.helper.CommentoHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.keybiz.antiNewsMMXIX.dto.CommentoDto;
import com.keybiz.antiNewsMMXIX.model.Commento;
import com.keybiz.antiNewsMMXIX.service.CommentoService;

@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping("commenti")
@RestController
@RequiredArgsConstructor
	public class CommentoController {

	private final CommentoService commentoService;

	@PostMapping
	public ResponseEntity<?> saveCommento(@RequestBody CommentoDto commentoDto) {
		Commento commento = commentoService.saveCommento(commentoDto);
		CommentoDto commentoResult = CommentoHelper.converti(commento);
		return ResponseEntity.created(null).body(commentoResult);
	}

	@PutMapping("{id}")
	public ResponseEntity<?> updateCommento(@RequestBody CommentoDto commentoUpdated, @PathVariable Long id){
		Optional<Commento> optCommento = commentoService.searchById(id);
		if(!optCommento.isPresent()){
			return ResponseEntity.notFound().build();
		}
		commentoUpdated.setId(id);
		commentoService.updateCommento(commentoUpdated);
		return ResponseEntity.ok().body(CommentoHelper.converti(optCommento.get()));
	}

	@GetMapping("{id}")
	public ResponseEntity<?> searchById(@PathVariable Long id){
		Optional<Commento> commento = commentoService.searchById(id);
		if(!commento.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(CommentoHelper.converti(commento.get()));
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void deleteById(@PathVariable Long id){ commentoService.deleteById(id); }
	
	@GetMapping
	public Collection<CommentoDto> findAll(){
		return commentoService.findAll().stream().map(CommentoHelper::converti).collect(Collectors.toList());
	}
}









