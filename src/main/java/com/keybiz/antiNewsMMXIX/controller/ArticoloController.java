package com.keybiz.antiNewsMMXIX.controller;

import java.util.Collection;
import java.util.Optional;

import com.keybiz.antiNewsMMXIX.helper.ArticoloHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.keybiz.antiNewsMMXIX.dto.ArticoloDto;
import com.keybiz.antiNewsMMXIX.model.Articolo;
import com.keybiz.antiNewsMMXIX.service.ArticoloService;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequiredArgsConstructor
@RequestMapping("articoli")
public class ArticoloController {

	private final ArticoloService articoloService;

	@PostMapping
	public ResponseEntity<?> saveArticolo(@RequestBody ArticoloDto articoloDto){
		Articolo articolo = articoloService.saveArticolo(articoloDto);
		ArticoloDto articoloResult = ArticoloHelper.converti(articolo);
		return ResponseEntity.created(null).body(articoloResult);
	}

	@PutMapping("{id}")
	public ResponseEntity<?> updateArticolo(@RequestBody ArticoloDto articoloUpdated, @PathVariable Long id){
		Optional<Articolo> optArticolo = articoloService.searchById(id);
		if(!optArticolo.isPresent()){
			return ResponseEntity.notFound().build();
		}
		articoloUpdated.setId(id);
		articoloService.updateArticolo(articoloUpdated);
		return ResponseEntity.ok().body(ArticoloHelper.converti(optArticolo.get()));
	}

	@GetMapping("{id}")
	public ResponseEntity<?> searchById(@PathVariable Long id){
		Optional<Articolo> articolo = articoloService.searchById(id);
		if(articolo.isPresent()) {
			return ResponseEntity.ok(ArticoloHelper.converti(articolo.get()));
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void deleteById(@PathVariable Long id){
		articoloService.deleteById(id);
	}
	
	@GetMapping
	public Collection<ArticoloDto> findAll(){
		return ArticoloHelper.converti(articoloService.findAll());
	}
}










