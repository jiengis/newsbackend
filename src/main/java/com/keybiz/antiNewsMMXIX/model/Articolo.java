package com.keybiz.antiNewsMMXIX.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Articolo extends AbstractModelContenuto {

	private String sezione;
	private Double prezzo;
	private boolean vendibile;

	@ManyToOne
	private Utente autoreArticolo;
}
