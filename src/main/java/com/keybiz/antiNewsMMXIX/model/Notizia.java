package com.keybiz.antiNewsMMXIX.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class Notizia extends AbstractModelContenuto {

	private String sottotitolo;
	private String riassunto;
	private Date dataFineVisual;
	private String importanza;

	@ManyToOne
	private Utente autoreNotizia;

	@OneToMany(mappedBy = "notiziaCommento")
	private Collection<Commento> commentiNotizia;
}
