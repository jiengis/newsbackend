package com.keybiz.antiNewsMMXIX.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Commento {

	@Id
	@GeneratedValue
	private Long id;
	
	private String testo;

	@ManyToOne
	private Utente commentatore;

	@ManyToOne
	private Notizia notiziaCommento;
}
