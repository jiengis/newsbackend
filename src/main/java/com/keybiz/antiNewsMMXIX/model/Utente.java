package com.keybiz.antiNewsMMXIX.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Utente {

	@Id
	@GeneratedValue
	private Long id;
	
	private String firstname;
	private String lastname;
	private String username;
	private String password;
	private Date dob;
	private String privileges;
	private Double contoVirtuale;

	@OneToMany(mappedBy = "autoreArticolo")
	private Collection<Articolo> articoliScritti;

	@OneToMany(mappedBy="autoreNotizia")
	private Collection<Notizia> notizieScritte;

	@OneToMany(mappedBy="commentatore")
	private Collection<Commento> commentiScritti;

}
